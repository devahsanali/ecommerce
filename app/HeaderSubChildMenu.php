<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class HeaderSubChildMenu extends Model {
   public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function header_sub_menu(){
        return $this->belongsTo('App\HeaderSubMenu', 'sub_header_id', 'id');
    }

}

