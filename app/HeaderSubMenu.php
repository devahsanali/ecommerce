<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class HeaderSubMenu extends Model {

	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function header_menu(){
        return $this->belongsTo('App\HeaderMenu', 'header_id', 'id');
    }

    public function header_sub_child_menu(){
        return $this->hasMany('App\HeaderSubChildMenu', 'sub_header_id', 'id');
    }

   
}

