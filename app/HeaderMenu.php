<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class HeaderMenu extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function sub_header_menu(){
        return $this->hasMany('App\HeaderSubMenu', 'header_id', 'id');
    }

   
}

