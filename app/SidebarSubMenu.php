<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SidebarSubMenu extends Model {

	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function sidebar_menu(){
        return $this->belongsTo('App\SidebarMenu', 'sidebar_id', 'id');
    }

   
}

