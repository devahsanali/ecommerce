<?php
function dt_format($date) {
   return date("M-d-Y", strtotime($date));
}

function dt_time_format($date) {
    $dt   = date("M-d-Y", strtotime($date));
    $time = date("H:i:s", strtotime($date));
   return [$dt, $time];
}

?>
