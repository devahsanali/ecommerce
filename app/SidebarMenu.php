<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SidebarMenu extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function sidebar_sub_menu(){
        return $this->hasMany('App\SidebarSubMenu', 'sidebar_id', 'id');
    }
   
}

