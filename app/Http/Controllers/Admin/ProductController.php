<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Http\Request;
use App\Product;
use App\HeaderMenu;
use App\HeaderSubMenu;
use App\HeaderSubChildMenu;
use App\SidebarMenu;
use App\SidebarSubMenu;
use App\Price;
use App\Image;
use App\Variant;
use App\Tag;
use App\WeightType;
//use App\Review;
use File;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function add()
    {   
        $header_menu            = $this->format_header_menu_data();
        //$sidebar_menu           = $this->format_sidebar_menu_data();
        $sidebar_menu           = SidebarMenu::with('sidebar_sub_menu')->get();
        $weight_type            = WeightType::get();
        $variant                = Variant::get();
        $tag                    = Tag::get();
        return view('admin.product.edit')->with('title', 'Create')->with(['header_menu' => $header_menu,  'sidebar_menu' => $sidebar_menu, 'weight_type' => $weight_type, 'variant' => $variant, 'tag' => $tag]);
    }

    protected function format_header_menu_data(){
      $menu = HeaderMenu::with('sub_header_menu.header_sub_child_menu')->get();
      $data = [];
      foreach($menu as $header){
          if(isset($header->sub_header_menu) && isset($header->sub_header_menu[0]->header_sub_child_menu) && !empty($header->sub_header_menu) && !empty($header->sub_header_menu[0]->header_sub_child_menu)){
            $row = [];
            $row['id']    =  $header->id;
            $row['title'] =  $header->title;
            $row['value'] =  $header->id;
            foreach($header->sub_header_menu as $sub_header){
              $sub_row = [];
              $sub_row['id']    =  $sub_header->id;
              $sub_row['title'] =  $sub_header->title;
              $sub_row['value'] =  $header->id.'_'.$sub_header->id;
              foreach($sub_header->header_sub_child_menu as $header_child){
                $child = [];
                $child['id']    =  $header_child->id;
                $child['title'] =  $header_child->title;
                $child['value'] =  $header->id.'_'.$sub_header->id."_".$header_child->id;
                $sub_row['children'][] = $child;
              }
              $row['children'][] = $sub_row;
            }
            $data[] = $row;
          }
      }
      return json_encode($data);
    }

    protected function format_sidebar_menu_data(){
      $menu = SidebarMenu::with('sidebar_sub_menu')->get();
      $data = [];
      foreach($menu as $sidebar){
          if(isset($sidebar->sidebar_sub_menu) && !empty($sidebar->sidebar_sub_menu)){
            $row = [];
            $row['id']    =  $sidebar->id;
            $row['title'] =  $sidebar->title;
            $row['value'] =  $sidebar->id;
            foreach($sidebar->sidebar_sub_menu as $sub_sidebar){
              $sub_row = [];
              $sub_row['id']    =  $sub_sidebar->id;
              $sub_row['title'] =  $sub_sidebar->title;
              $sub_row['value'] =  $sidebar->id.'_'.$sub_sidebar->id;
              $row['children'][] = $sub_row;
            }
            $data[] = $row;
          }
      }
      return json_encode($data);
    }

    public function insert(Request $request){
       $today_stamp                 = date("Y-m-d H:i:s");
       $file                        = explode("~~",$request->fileName);
       if(isset($request->duplicate)){
         $file = $this->productImageDuplicate($file);
       }
       $product                     = new Product;
       $product->title              = $request->title; 
       $product->permalink          = $request->permalink; 
       $product->weight             = $request->weight;
       $product->weight_type        = $request->weight_type;
       $product->discount           = ($request->discount > 0)? $request->discount : 0;
       $product->short_description  = $request->short_description;
       $product->brief_description  = $request->brief_description;
       $product->category_id        = $request->category_id;
       $product->header_id          = $request->header_id;
       $product->menu_id            = $request->menu_id;
       $product->save();

       $product_id = $product->id;

       $product_price                       = new Price;
       $product_price->price                = $request->price;
       $product_price->country_id           = $request->country_id;
       $product_price->product_id           = $product_id;
       $product_price->save();
      
      for($i= 0; $i < count($file)-1; $i++){
          $upload = Image::insert(['image' => $file[$i], 'product_id' => $product_id, 'created_at' => $today_stamp, 'updated_at' => $today_stamp ]);
      }

       
      $vari_price = explode(",",$request->variant_price);
      
      if(strlen($request->value_1) > 0){
         $value = explode(",",$request->value_1);
         $var_category = ProductVariantCategory::where('id', $request->option_1)->first();
         $data = [
                'value' => $value,
                'product_id' => $product_id,
                'category_id' => $var_category->id,
                'is_price' => $var_category->is_price,
                'vari_price' => $vari_price,
                'country_id' => $request->country_id
          ];
         $this->variant_insert($data);
      }

      if(strlen($request->value_2) > 0){
         $value = explode(",",$request->value_2);
         $var_category = ProductVariantCategory::where('id', $request->option_2)->first();
         $data = [
                'value' => $value,
                'product_id' => $product_id,
                'category_id' => $var_category->id,
                'is_price' => $var_category->is_price,
                'vari_price' => $vari_price,
                'country_id' => $request->country_id
          ];
         $this->variant_insert($data);
      }
                         
     return "OK";    
    }

    public function variant_insert(Array $val){
       for($i = 0; $i<sizeof($val['value']); $i++){
            $variant                      = new ProductVariant;
            $variant->title               = $val['value'][$i]; 
            $variant->product_id          = $val['product_id'];
            $variant->variant_category_id = $val['category_id'] ;
            $variant->save();
            $variant_price                       = new Price;
            $variant_price->price                = ($val['is_price'] == 1) ? (isset($val['vari_price'][$i])? (int)$val['vari_price'][$i] : 0) : 0;
            $variant_price->country_id           = $val['country_id'];
            $variant_price->product_id           = $val['product_id'];
            $variant_price->variant_id           = $variant->id;
            $variant_price->save();
         }
    }

    public function productImageUpload(Request $request){
      $file = $request->file('file');
      $target_dir = env('PRODUCT_IMAGES');
      $target_file = $target_dir . basename($_FILES["file"]["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      
      $filename  = basename($_FILES['file']['name']);
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $new       = $filename;
      move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('products')."/{$new}");
          
      return $new;
    }

    public function productImageDuplicate($files){
      $files = $files;
      $new_files = Array();
      foreach($files as $file){
        if($file != ""){
           $old = storage_path('products/'.$file);
           $fileExtension = \File::extension($old);
           $new = time()."_duplicate.".$fileExtension;
           $newPathWithName =  storage_path('products/'.$new);
           \File::copy($old , $newPathWithName);
           $new_files[] = $new;
        }
      }
      $new_files[] = "";
      return $new_files;
    }

    public function removeImage(){
      $file = input::get('file');
      $delete = File::delete(env('PRODUCT_IMAGES').$file);
      Image::where('image', $file)->delete();
      return 'true';
    }

    public function manage(){
      $product = Product::with(['category', 'category_header', 'menu', 'product_images'])->orderBy('id', 'desc')->paginate(25);
      return View('product.manage', compact('product'))->with('title', 'Manage Product');      
    }

    public function details(Request $request){
      $product = Product::with('category', 'category_header', 'menu', 'product_images', 'price', 'weight_types','product_variant.product_variant_category')/*->with('review')*/->where('id', $request->id)->first();
        return View('product.detail', compact('product'))->with(['title' => 'Product Details' ]);      
    }
 
    public function edit(Request $request){
      $route_name       = $request->route()->getName();
      $category         = Category::get();
      $category_type    = CategoryHeader::get();
      $menu             = CategoryType::get();
      $weight_type      = WeightType::get();
      $variant_category = ProductVariantCategory::get();
      $product   = Product::with('product_images', 'price', 'product_variant.product_variant_category', 'product_variant.price')->where('id', $request->id)->first();
      if($route_name == "product.duplicate"){
        return View('product.duplicate', compact('product', 'category', 'category_type', 'menu', 'weight_type', 'variant_category'))->with('title', 'Duplicate product');  
     }else{
        return View('product.edit', compact('product', 'category', 'category_type', 'menu', 'weight_type', 'variant_category'))->with('title', 'Edit product');      
     }
    }

    public function update(Request $request){
       $today_stamp          = date("Y-m-d H:i:s");
       $product_id           = $request->id;
       $file                 = explode("~~",$request->fileName);
       Product::where('id', $product_id)->update([
       'title'              => $request->title,
       'permalink'          => $request->permalink,
       'weight'             => $request->weight,
       'weight_type'        => $request->weight_type,
       'discount'           => ($request->discount > 0)? $request->discount : 0,
       'short_description'  => $request->short_description,
       'brief_description'  => $request->brief_description,
       'category_id'        => $request->category_id,
       'header_id'          => $request->header_id,
       'menu_id'            => $request->menu_id
       ]); 

       Price::where('product_id', $product_id)->forceDelete();     

       $product_price                       = new Price;
       $product_price->price                = $request->price;
       $product_price->country_id           = $request->country_id;
       $product_price->product_id           = $product_id;
       $product_price->save();

       Image::where('product_id', $product_id)->forceDelete();     
       for($i= 0; $i < count($file)-1; $i++){
          $count = Image::where('image', $file[$i])->count();
          if($count < 1){
              $upload = Image::insert(['image' => $file[$i], 'product_id' => $product_id, 'created_at' => $today_stamp, 'updated_at' => $today_stamp ]);
          }
       }
       
       $vari_price = explode(",",$request->variant_price);

       if(ProductVariant::where('product_id', $product_id)->count() > 0){
         ProductVariant::where('product_id', $product_id)->forceDelete();
       } 
       if(strlen($request->value_1) > 0){
         $value = explode(",",$request->value_1);
         $var_category = ProductVariantCategory::where('id', $request->option_1)->first();
         $data = [
                'value' => $value,
                'product_id' => $product_id,
                'category_id' => $var_category->id,
                'is_price' => $var_category->is_price,
                'vari_price' => $vari_price,
                'country_id' => $request->country_id
          ];
         $this->variant_insert($data);
       }

       if(strlen($request->value_2) > 0){
         $value = explode(",",$request->value_2);
         /*ProductVariant::where('product_id', $product_id)->where('variant_category_id', $request->option_2)->forceDelete();*/
         $var_category = ProductVariantCategory::where('id', $request->option_2)->first();
         $data = [
                'value' => $value,
                'product_id' => $product_id,
                'category_id' => $var_category->id,
                'is_price' => $var_category->is_price,
                'vari_price' => $vari_price,
                'country_id' => $request->country_id
          ];
         $this->variant_insert($data);
       }

       /*if(strlen($request->size) > 0){
         ProductVariant::where('product_id', $product_id)->where('variant_category_id', 1)->forceDelete();
         $size = explode(",",$request->size);
         
        for($i = 0; $i<sizeof($size); $i++){
            $variant                      = new ProductVariant;
            $variant->title               = $size[$i]; 
            $variant->product_id          = $product_id;
            $variant->variant_category_id = 1 ;
            $variant->save();
            
            $variant_price                       = new Price;
            $variant_price->price                = isset($vari_price[$i])? $vari_price[$i] : 0;
            $variant_price->country_id           = $request->country_id;
            $variant_price->product_id           = $product_id;
            $variant_price->variant_id           = $variant->id;
            $variant_price->save();
         }
      }

      if(strlen($request->color) > 0){
         ProductVariant::where('product_id', $product_id)->where('variant_category_id', 2)->forceDelete();
         $color = explode(",",$request->color);         
        for($i = 0; $i<sizeof($color); $i++){
            $variant                      = new ProductVariant;
            $variant->title               = $color[$i]; 
            $variant->product_id          = $product_id;
            $variant->variant_category_id = 2;
            $variant->save();
            
            $variant_price                       = new Price;
            $variant_price->price                = 0;
            $variant_price->country_id           = $request->country_id;
            $variant_price->product_id           = $product_id;
            $variant_price->variant_id           = $variant->id;
            $variant_price->save();
         }
      }*/
                         
     return "OK";    
    }

    public function delete(Request $request){
      $product_id = $request->id;
      Product::where('id', $product_id)->delete();
      Price::where('product_id', $product_id)->delete();
      Image::where('product_id', $product_id)->delete();     
      ProductVariant::where('product_id', $product_id)->delete();
      return 'OK';
    }

    public function validate_permalink(Request $request){
        if(isset($request->id)){
          return Product::where('permalink', $request->permalink)->where('id', '!=', $request->id)->count();
        }
        return Product::where('permalink', $request->permalink)->count();
    }



   /*public function searchFile(Request $request){
     $file = $request->file('file');
     $result  = array();
      if ( false!==$file ) {
        if ( '.'!=$file && '..'!=$file) {      
           $obj['name'] = $file;
           $obj['size'] = filesize(env('PRODUCT_IMAGES').$file);
           $result[] = $obj;
        }
      }
      header('Content-type: text/json');              
      header('Content-type: application/json');
      echo json_encode($result);  
   }

  
    public function findItemType(Request $request){
       $type = ItemType::where('item_id', $request->id)->get();
       return $type->toArray();
    }

    */
}

