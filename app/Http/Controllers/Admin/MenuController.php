<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\HeaderMenu;
use App\HeaderSubMenu;
use App\HeaderSubChildMenu;
use App\FooterMenu;
use App\FooterSubMenu;
use App\SidebarMenu;
use App\SidebarSubMenu;
class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function header_menu(){   
        $data = HeaderMenu::orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/header_menu/manage')->with(['title' => 'Header Menu', 'data' => $data]); 
    }
    public function header_menu_add(Request $request){
        return view('admin/menu/header_menu/edit')->with(['title' => 'Create']);   
    }

    public function header_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = HeaderMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.header.menu')];
        }else{
            $data = new HeaderMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.header.menu')];
        }
    }

    public function header_menu_edit(Request $request){
        $data = HeaderMenu::where('id', $request->id)->first();
        return view('admin/menu/header_menu/edit')->with(['title' => 'Edit', 'data' => $data]);   
    }

    public function header_menu_delete(Request $request){
      $id = $request->id;
      HeaderMenu::where('id', $id)->delete();
      HeaderSubMenu::where('header_id', $id)->delete();

      return 'success';
    }

    public function header_menu_status(Request $request){
      $id = $request->id;
      $data = HeaderMenu::where('id', $id)->first();
      $status = ($data->is_active == 1) ? 0 : 1;
      HeaderMenu::where('id', $id)->update(['status' => $status]);
      return 'success';
    }


    public function header_sub_menu($id){   
        $data = HeaderSubMenu::with('header_menu')->where('header_id', $id)->orderBy('id', 'desc')->paginate(25);
            return view('admin/menu/header_menu/sub_header/manage')->with(['title' => 'Sub Header Menu', 'data' => $data, 'header_id' => $id, ]); 
    }
    public function header_sub_menu_add(Request $request){
        $header_menu = HeaderMenu::get();
        return view('admin/menu/header_menu/sub_header/edit')->with(['title' => 'Create', 'header_menu' => $header_menu, 'header_id' => $request->id]);   
    }

    public function header_sub_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
            'header_id' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = HeaderSubMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
                'header_id' => $request->header_id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.header.sub.menu',$request->header_id )];
        }else{
            $data = new HeaderSubMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->header_id = $request->header_id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.header.sub.menu', $request->header_id)];
        }
    }

    public function header_sub_menu_edit(Request $request){
        $data = HeaderSubMenu::with('header_menu')->where('id', $request->id)->first();
        $header_menu = HeaderMenu::get();
        return view('admin/menu/header_menu/sub_header/edit')->with(['title' => 'Edit', 'data' => $data, 'header_menu' => $header_menu, 'header_id'=> $request->header_id]);   
    }

    public function header_sub_menu_delete(Request $request){
      $id = $request->id;
      HeaderSubMenu::where('id', $id)->delete();
      return 'success';
    }

    public function header_sub_child_menu(Request $request){
        if(isset($request->id) && !empty($request->id) && isset($request->header_id) && !empty($request->header_id))
        {   
            $id = $request->id;
            $header_id = $request->header_id; 
            $data = HeaderSubChildMenu::with('header_sub_menu')->where('sub_header_id', $id)->orderBy('id', 'desc')->paginate(25);
                return view('admin/menu/header_menu/sub_header_child/manage')->with(['title' => 'Sub Header Child Menu', 'data' => $data, 'sub_header_id' => $id,  'header_id' => $header_id]); 
        }else{
            return Redirect::back()->withErrors(['Invalid URL']);
        }
    }
    public function header_sub_child_menu_add(Request $request){
        if(isset($request->id) && !empty($request->id) && isset($request->header_id) && !empty($request->header_id))
        {   
            $id = $request->id;
            $header_id = $request->header_id; 
            $header_menu = HeaderMenu::get();
            $sub_header_menu = HeaderSubMenu::get();
            return view('admin/menu/header_menu/sub_header_child/edit')->with(['title' => 'Create', 'header_menu' => $header_menu, 'sub_header_menu' => $sub_header_menu, 'sub_header_id' => $id, 'header_id' => $header_id]);  
        }else{
            return Redirect::back()->withErrors(['Invalid URL']);
        } 
    }

    public function header_sub_child_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
            'header_id' => 'required',
            'sub_header_id' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = HeaderSubChildMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
                'header_id' => $request->header_id,
                'sub_header_id' => $request->sub_header_id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.header.sub.child.menu',[$request->sub_header_id, 'header_id' => $request->header_id] )];
        }else{
            $data = new HeaderSubChildMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->header_id = $request->header_id;
            $data->sub_header_id = $request->sub_header_id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.header.sub.child.menu', [$request->sub_header_id, 'header_id' => $request->header_id])];
        }
    }

    public function header_sub_child_menu_edit(Request $request){
        if(isset($request->id) && !empty($request->id) && isset($request->sub_header_id) && !empty($request->sub_header_id) && isset($request->header_id) && !empty($request->header_id))
        {  
        $data = HeaderSubChildMenu::with('header_sub_menu')->where('id', $request->id)->first();
        $header_menu = HeaderMenu::get();
        $sub_header_menu = HeaderSubMenu::get();

        return view('admin/menu/header_menu/sub_header_child/edit')->with(['title' => 'Edit', 'data' => $data, 'header_menu' => $header_menu, 'sub_header_menu' => $sub_header_menu, 'header_id'=> $request->header_id, 'sub_header_id'=> $request->sub_header_id]);
        }else{
            return Redirect::back()->withErrors(['Invalid URL']);
        }   
    }

    public function header_sub_child_menu_delete(Request $request){
       if(isset($request->id) && !empty($request->id) && isset($request->sub_header_id) && !empty($request->sub_header_id) && isset($request->header_id) && !empty($request->header_id))
        {  
          $id = $request->id;
          HeaderSubChildMenu::where('id', $id)->delete();
          return 'success';
        }else{
            return 'Invalid URL';
        }   
    }

    public function footer_menu(){   
        $data = FooterMenu::orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/footer_menu/manage')->with(['title' => 'Footer Menu', 'data' => $data]); 
    }
    public function footer_menu_add(Request $request){
        return view('admin/menu/footer_menu/edit')->with(['title' => 'Create']);   
    }

    public function footer_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = FooterMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.footer.menu')];
        }else{
            $data = new FooterMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.footer.menu')];
        }
    }

    public function footer_menu_edit(Request $request){
        $data = FooterMenu::where('id', $request->id)->first();
        return view('admin/menu/footer_menu/edit')->with(['title' => 'Edit', 'data' => $data]);   
    }

    public function footer_menu_delete(Request $request){
      $id = $request->id;
      FooterMenu::where('id', $id)->delete();
      FooterSubMenu::where('footer_id', $id)->delete();

      return 'success';
    }


    public function footer_sub_menu($id){   
        $data = FooterSubMenu::with('footer_menu')->where('footer_id', $id)->orderBy('id', 'desc')->paginate(25);
            return view('admin/menu/footer_menu/sub_footer/manage')->with(['title' => 'Sub Footer Menu', 'data' => $data, 'footer_id' => $id, ]); 
    }
    public function footer_sub_menu_add(Request $request){
        $footer_menu = FooterMenu::get();
        return view('admin/menu/footer_menu/sub_footer/edit')->with(['title' => 'Create', 'footer_menu' => $footer_menu, 'footer_id' => $request->id]);   
    }

    public function footer_sub_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
            'footer_id' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = FooterSubMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
                'footer_id' => $request->footer_id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.footer.sub.menu',$request->footer_id )];
        }else{
            $data = new FooterSubMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->footer_id = $request->footer_id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.footer.sub.menu', $request->footer_id)];
        }
    }

    public function footer_sub_menu_edit(Request $request){
        $data = FooterSubMenu::with('footer_menu')->where('id', $request->id)->first();
        $footer_menu = FooterMenu::get();
        return view('admin/menu/footer_menu/sub_footer/edit')->with(['title' => 'Edit', 'data' => $data, 'footer_menu' => $footer_menu, 'footer_id'=> $request->footer_id]);   
    }

    public function footer_sub_menu_delete(Request $request){
      $id = $request->id;
      FooterSubMenu::where('id', $id)->delete();
      return 'success';
    }

    public function sidebar_menu(){   
        $data = SidebarMenu::orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/sidebar_menu/manage')->with(['title' => 'Sidebar Menu', 'data' => $data]); 
    }
    public function sidebar_menu_add(Request $request){
        return view('admin/menu/sidebar_menu/edit')->with(['title' => 'Create']);   
    }

    public function sidebar_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = SidebarMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.sidebar.menu')];
        }else{
            $data = new SidebarMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.sidebar.menu')];
        }
    }

    public function sidebar_menu_edit(Request $request){
        $data = SidebarMenu::where('id', $request->id)->first();
        return view('admin/menu/sidebar_menu/edit')->with(['title' => 'Edit', 'data' => $data]);   
    }

    public function sidebar_menu_delete(Request $request){
      $id = $request->id;
      SidebarMenu::where('id', $id)->delete();
      SidebarSubMenu::where('sidebar_id', $id)->delete();

      return 'success';
    }


    public function sidebar_sub_menu($id){   
        $data = SidebarSubMenu::with('sidebar_menu')->where('sidebar_id', $id)->orderBy('id', 'desc')->paginate(25);
            return view('admin/menu/sidebar_menu/sub_sidebar/manage')->with(['title' => 'Sub Footer Menu', 'data' => $data, 'sidebar_id' => $id, ]); 
    }
    public function sidebar_sub_menu_add(Request $request){
        $sidebar_menu = SidebarMenu::get();
        return view('admin/menu/sidebar_menu/sub_sidebar/edit')->with(['title' => 'Create', 'sidebar_menu' => $sidebar_menu, 'sidebar_id' => $request->id]);   
    }

    public function sidebar_sub_menu_post(Request $request){
        $request->validate([
            'title' => 'required',
            'sidebar_id' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = SidebarSubMenu::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
                'sidebar_id' => $request->sidebar_id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.sidebar.sub.menu',$request->sidebar_id )];
        }else{
            $data = new SidebarSubMenu();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->sidebar_id = $request->sidebar_id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.sidebar.sub.menu', $request->sidebar_id)];
        }
    }

    public function sidebar_sub_menu_edit(Request $request){
        $data = SidebarSubMenu::with('sidebar_menu')->where('id', $request->id)->first();
        $sidebar_menu = SidebarMenu::get();
        return view('admin/menu/sidebar_menu/sub_sidebar/edit')->with(['title' => 'Edit', 'data' => $data, 'sidebar_menu' => $sidebar_menu, 'sidebar_id'=> $request->sidebar_id]);   
    }

    public function sidebar_sub_menu_delete(Request $request){
      $id = $request->id;
      SidebarSubMenu::where('id', $id)->delete();
      return 'success';
    }

}
