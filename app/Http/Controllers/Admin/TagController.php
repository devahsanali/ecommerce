<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Tag;
class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function index(){   
        $data = Tag::orderBy('id', 'desc')->paginate(25);
        return view('admin/tag/manage')->with(['title' => 'Tag', 'data' => $data]); 
    }
    public function add(Request $request){
        return view('admin/tag/edit')->with(['title' => 'Create']);   
    }

    public function post(Request $request){
        $request->validate([
            'title' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = Tag::where('id', $request->id)->update([
                'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            return ["success" => "Successfully Updated.", "redirect" => route('admin.tag.manage')];
        }else{
            $data = new Tag();
            $data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            return ["success" => "Successfully Added.", "redirect" => route('admin.tag.manage')];
        }
    }

    public function edit(Request $request){
        $data = Tag::where('id', $request->id)->first();
        return view('admin/tag/edit')->with(['title' => 'Edit', 'data' => $data]);   
    }

    public function delete(Request $request){
      $id = $request->id;
      Tag::where('id', $id)->delete();
      return 'success';
    }
  }