@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Menu</li>
            <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.header.menu')}}">Header Menu</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
                <form id="form-create" class="form validate ajaxForm" action="{{route('admin.header.menu.post')}}" method="post"> 
                 @csrf
                  
                  <p>Title</p>   
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title" value="{{@$data->title}}">
                  </div>
                  <div class="input-group mb-3">
                    <input type="checkbox" id="basic_checkbox_2" name="is_active" {{ (isset($data->is_active) && $data->is_active == 1)? 'checked' : ''}} />
                    <label for="basic_checkbox_2">Active</label>
                  </div>
                  <input type="hidden" name="id" value="{{@$data->id}}">
                  <button type="button" class="btn btn-primary create ajaxFormSubmit btn-lg btn-block waves-effect waves-light m-1">Submit</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->

@endsection
@section('footer')
<script>
   $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        title:{
          required: true
        },
      }, 
      messages: {
        category_name: "This field is required.",
        category_is_active: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
  });
</script>

@endsection
