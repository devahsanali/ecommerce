@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">Manage {{@$title}}</h4>
            <ol class="breadcrumb">
              <li class="breadcrumb-item active" aria-current="page">Menu</li>
              <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
              <li class="breadcrumb-item active" aria-current="page">
               <a href="{{route('admin.header.menu')}}">Manage</a>
             </li> 
            </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <a href="{{route('admin.header.menu.add')}}">
            <button type="button" class="pull-right btn btn-secondary waves-effect waves-light m-1"  title="Add"> 
            Add
            </button>
          </a>
        </div>
        <div class="col-lg-12">
            @include('includes.alert')
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">All {{@$title}}</h5>
              <div class="table">
               <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col" colspan="2">Title</th>
                      <th scope="col">Date</th>
                      <th scope="col">Status</th>
                      <th scope="col">Created By</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  @php($i= $data->perPage() * ($data->currentPage() - 1)+1)
                  <tbody>
                    @if(isset($error))
                    <tr>
                      <th colspan="6">
                        Data Not Found
                      </th>
                    </tr>
                    @else
                    @foreach($data as $dt)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td class="pro-title" colspan="2">{{$dt->title}}</td>
                      <td>{{dt_format($dt->created_at)}}</td>
                      <td>
                        @php($status = ($dt->is_active == 1)? 'checked' :'')
                         <!-- onchange="update_status('{{$dt->id}}', '{{route('admin.header.menu.status')}}')" -->
                        <!-- <input type="checkbox" {{$status}} data-size="small" data-id="{{$dt->id}}" data-url="{{route('admin.header.menu.status')}}" class="js-switch update_status"/> -->  
                        {{($dt->is_active == 1)? 'Active' : 'Deactivated'}}
                      </td>
                      <td>{{$dt->user->name}}</td>
                     
                      <td class="action-buttons">
                        <a href="{{route('admin.header.sub.menu', $dt->id)}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="View Sub Header"> <i class="fa fa-eye"></i> </button>
                        </a>
                        <a href="{{route('admin.header.menu.edit', $dt->id)}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="Edit"> <i class="fa fa-pencil"></i></button>
                        </a>
                        
                         <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.header.menu.delete')}}')" title="Delete"> <i class="fa fa-trash"></i></button>
                         
                      </td>
                    </tr>
                    
                    @endforeach
                    @endif
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
 <?php if(isset($data)){?>    
 {!! $data->render() !!}
 <?php }?>
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection