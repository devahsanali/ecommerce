@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">Manage Product</h4>
            <ol class="breadcrumb">
              <li class="breadcrumb-item active" aria-current="page">Product</li>
              <li class="breadcrumb-item active" aria-current="page">Manage</li> 
            </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
            @include('product.includes.alert')
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">All Products</h5>
              <div class="table">
               <table class="table table-striped">
                 <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1);?>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col" colspan="2">Title</th>
                      <th scope="col">Category</th>
                      <th scope="col">Type</th>
                      <th scope="col">Collection</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(isset($error)){?>
                    <tr>
                      <th colspan="6">
                        No Result Found
                      </th>
                    </tr>
                    <?php } else{ ?>
                    @foreach($product as $products)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td>
                        @if(isset($_GET['keyword']))
                        <img width="40" src="{{env('PRODUCT_IMAGES')}}{{$products->product_images[0]->image}}"/>
                        @else
                        <img width="40" src="../{{env('PRODUCT_IMAGES')}}{{$products->product_images[0]->image}}"/>
                        @endif
                      </td>
                      <td class="pro-title">{{$products->title}}</td>
                      <td>{{$products->category[0]->title}}</td>
                      <td>{{$products->category_header[0]->title}}</td>
                      <td>{{$products->menu[0]->title}}</td>
                      <td class="action-buttons">
                        <a href="{{route('product.details').'?id='.$products->id}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="View Detail"> <i class="fa fa-eye"></i> </button>
                        </a>
                        <a href="{{route('product.edit').'?id='.$products->id}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1"> <i class="fa fa-pencil"></i></button>
                        </a>
                         <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="productDelete({{$products->id}})"> <i class="fa fa-trash"></i></button>
                         </a>
                      </td>
                    </tr>
                    @endforeach
                    <?php } ?>
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
   @include('product.includes.productJs')
 <?php if(isset($product)){?>    
 {!! $product->render() !!}
 <?php }?>
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection