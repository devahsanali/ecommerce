@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Product</li>
            <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
              <form enctype="multipart/form-data" action="{{route('product.upload.image')}}" class="dropzone" id="dropzone">
                  <div class="fallback dropzone">
                    <input name="file[]" id="file" type="file" multiple="multiple">
                  </div>
                  <input type="hidden" name="new_name" value="">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form> 
              <br/>
                 <form id="product-create"> 
                  <div class="row">
                      <div class="col-md-6">
                        <p>Title</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" name="title">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>Haeder Menu</p>  
                        <div class="input-group mb-3"> 
                          <div class="container">
                            <div class="treeSelector "></div>
                          </div>
                        </div>
                      </div>
                  </div>  
                  <input type="hidden" name="fileName" id="fileName" value=""/>
                  <div class="row">
                      <div class="col-md-6">
                            <div class="form-group">
                              <label>Sidebar Menu</label>
                              <select class="form-control multiple-select select2" multiple="multiple" name="sidebar_menu[]">
                                @foreach($sidebar_menu as $v)
                                  <optgroup label="{{$v->title}}">
                                      @foreach($v->sidebar_sub_menu as $k)
                                        <option value="{{$v->id}}_{{$k->id}}">{{$k->title}}</option>
                                      @endforeach
                                  </optgroup>
                                @endforeach
                              </select>
                            </div>
                      </div>
                      <div class="col-md-6">
                          <p>Price</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" name="price">
                            <input type="hidden" value="1" class="form-control invt" name="country_id">
                          </div>
                      </div>
                  </div>
                   <div class="row">
                      <div class="col-md-6">
                        <p>Weight</p>   
                        <div class="input-group mb-3">
                            <input type="text" name="weight" class="form-control">
                            <div class="input-group-append">
                              <select name="weight_type" class="btn btn-outline-primary joint-select">
                                @foreach($weight_type as $wtype)
                                 <option value="{{$wtype->id}}">{{$wtype->title}}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                          <p>Discount</p>   
                          <div class="input-group mb-3">
                             <input type="text" name="discount" class="form-control">
                          </div>
                      </div>
                  </div>
                   <div class="form-group">
                      <label>Select Tag</label>
                      <select class="form-control multiple-select" name="tag[]" multiple="multiple">
                         @foreach($tag as $v)
                            <option value="{{$v->id}}">{{$v->title}}</option>
                         @endforeach
                      </select>
                    </div>
                  <p>Description</p>   
                  <textarea class="summernoteEditor" class="form-control invt" name="short_description" id="short_description">
                  </textarea>
                  <p>Brief Detail</p>   
                  <textarea class="summernoteEditor" name="brief_description" id="brief_description">
                  </textarea>
                  <div class="card border border-primary">
                    <div class="card-header">
                        <i class="fa fa-plus"></i>
                        <button  type="button" class="btn btn-link shadow-none text-primary collapsed" data-toggle="collapse" data-target="#collapse-variant" aria-expanded="false" aria-controls="collapse-variant">
                         Add Variant
                        </button>
                    </div>
                    <div id="collapse-variant" class="collapse">
                      <div class="card-body">
                         <p>Priceable Variant</p>   
                         <div class="input-group mb-3" name="bodyStyle">
                             <div class="demo-radio-button">
                                  @foreach($variant as $v)
                                    <option value="{{$v->id}}"></option>
                                    <input name="variant_has_price" type="radio" id="variant_{{$v->id}}" value="{{$v->id}}"/>
                                    <label for="variant_{{$v->id}}">{{$v->title}}</label>
                                  @endforeach                                
                              </div>
                          </div>
                           <!-- <p>Size</p> -->
                            <div class="input-group mb-3" name="bodyStyle">
                              <div class="input-group-append select_parent">
                                  <select name="variant_1" class="select_menu btn btn-outline-primary joint-select">
                                     <option value="0">Choose...</option>
                                     @foreach($variant as $v)
                                      <option value="{{$v->id}}">{{$v->title}}</option>
                                     @endforeach
                                  </select>
                              </div>   
                              <input type="text" data-role="tagsinput" class="form-control invt" id="size" name="value_1"/>
                            </div>
                            <!-- <p>Color</p> -->   
                            <div class="input-group mb-3" name="bodyStyle">
                               <div class="input-group-append select_parent">
                                  <select name="variant_2" class="select_menu btn btn-outline-primary joint-select">
                                     <option value="0">Choose...</option>
                                     @foreach($variant as $v)
                                      <option value="{{$v->id}}">{{$v->title}}</option>
                                     @endforeach
                                  </select>
                               </div> 
                               <input type="text" data-role="tagsinput" class="form-control invt" id="color" name="value_2"/>
                             </div>
                              <div class="input-group mb-3" name="bodyStyle">
                               <div class="input-group-append select_parent">
                                  <select name="variant_3" class="select_menu btn btn-outline-primary joint-select">
                                     <option value="0">Choose...</option>
                                     @foreach($variant as $v)
                                      <option value="{{$v->id}}">{{$v->title}}</option>
                                     @endforeach
                                  </select>
                               </div> 
                               <input type="text" data-role="tagsinput" class="form-control invt" id="color" name="value_2"/>
                             </div>
                             <p>Prices</p>   
                             <div class="input-group mb-3" name="bodyStyle">
                               <input type="text" data-role="tagsinput" class="form-control invt" id="variant_price" name="variant_price">
                             </div>
                             
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary create btn-lg btn-block waves-effect waves-light m-1">Create</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
@endsection
@section('footer')
<script>
 jQuery(document).ready(function ($) {
      var data = '{!! $header_menu !!}';
      var rootNode = JSON.parse(data);
      var sidebar = '{!! $sidebar_menu !!}';
      var rootNode2 = JSON.parse(sidebar);
     
      $('div.treeSelector').treeSelector(rootNode, [], function (e, values) {
        console.info('onChange',  values);
        console.log(values[2]);
      }, {
        checkWithParent: true,
        titleWithParent: true,
        notViewClickParentTitle: true
      })
      $('div.treeSelector2').treeSelector(rootNode2, [], function (e, values) {
        console.info('onChange',  values);
      }, {
        checkWithParent: true,
        titleWithParent: true,
        notViewClickParentTitle: true
      })
    })
</script>
@endsection
