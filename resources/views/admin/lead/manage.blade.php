@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">Leads</h4>
            <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item active" aria-current="page">Lead</li> -->
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">All Leads</h5>
              <div class="table">
               <table class="table table-striped">
                 <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1);?>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Email</th>
                      <th scope="col">Message</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $leads)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td>{{$leads->name}}</td>
                      <td>{{$leads->phone}}</td>
                      <td>{{$leads->email}}</td>
                      <td>
                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1 trigger_modal" data-toggle="modal" data-target="#modal"> <i class="fa fa-search"></i> </button>
                        <textarea class="message" style="display: none;">{!! $leads->message !!}</textarea> 
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
     
 {!! $data->render() !!}
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection
@section('footer')
<script>
  $('.trigger_modal').click(function(event) {
    var message = $(this).siblings('.message').text();
    var data = "<p>"+message+"</p>";
    $('.modal_body').html(data);
    $('.modal_header_icon').html('<i class="fa fa-envelope"></i>');
    $('.modal_header_text').html('Message');
  });
</script>
@endsection
