<div class="modal fade" id="modal">
  <div class="modal-dialog">
    <div class="modal-content border-light">
      <div class="modal-header bg-light">
        <h5 class="modal-title text-dark"> <span class="modal_header_icon"></span> <span class="modal_header_text"></span></h5>
        <button type="button" class="close text-dark" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body modal_body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-inverse-light" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
      </div>
    </div>
  </div>
</div>