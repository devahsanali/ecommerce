<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website/index');
});
Route::get('/detail', function () {
    return view('website/details');
});
Route::get('/contact', function () {
    return view('website/contact');
});
Route::get('/cart', function () {
    return view('website/cart');
});
Route::get('/checkout', function () {
    return view('website/checkout');
});
Route::get('/sign-in', function () {
    return view('website//customer/signin');
});
Route::get('/login', function () {
    return view('auth/login');
});
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Auth::routes();
Route::group(['prefix' => 'admin', 'middleware' => ['role:admin', 'auth'],], function () {
	Route::get('home/', 'Admin\HomeController@index')->name('admin.home');
			Route::get('/sub-menu', 'Admin\MenuController@header_menu')->name('admin.header.sub.menu');

	//header menu
	Route::group(['prefix' => 'menu'], function () {
		Route::group(['prefix' => 'header-menu'], function () {
			Route::get('/', 'Admin\MenuController@header_menu')->name('admin.header.menu');
			Route::get('add', 'Admin\MenuController@header_menu_add')->name('admin.header.menu.add');
			Route::post('post', 'Admin\MenuController@header_menu_post')->name('admin.header.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@header_menu_edit')->name('admin.header.menu.edit');
		    Route::post('delete', 'Admin\MenuController@header_menu_delete')->name('admin.header.menu.delete');
		    Route::post('status', 'Admin\MenuController@header_menu_status')->name('admin.header.menu.status');
		});

		Route::group(['prefix' => 'sub-header-menu'], function () {
			Route::get('/{id}', 'Admin\MenuController@header_sub_menu')->name('admin.header.sub.menu');
			Route::get('add/{id}', 'Admin\MenuController@header_sub_menu_add')->name('admin.header.sub.menu.add');
			Route::post('post', 'Admin\MenuController@header_sub_menu_post')->name('admin.header.sub.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@header_sub_menu_edit')->name('admin.header.sub.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@header_sub_menu_delete')->name('admin.header.sub.menu.delete');
		    Route::post('status', 'Admin\MenuController@header_sub_menu_status')->name('admin.header.sub.menu.status');
		});

		Route::group(['prefix' => 'sub-header-child-menu'], function () {
			Route::get('/{id}', 'Admin\MenuController@header_sub_child_menu')->name('admin.header.sub.child.menu');
			Route::get('add/{id}', 'Admin\MenuController@header_sub_child_menu_add')->name('admin.header.sub.child.menu.add');
			Route::post('post', 'Admin\MenuController@header_sub_child_menu_post')->name('admin.header.sub.child.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@header_sub_child_menu_edit')->name('admin.header.sub.child.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@header_sub_child_menu_delete')->name('admin.header.sub.child.menu.delete');
		    Route::post('status', 'Admin\MenuController@header_sub_child_menu_status')->name('admin.header.sub.child.menu.status');
		});

		Route::group(['prefix' => 'footer-menu'], function () {
			Route::get('/', 'Admin\MenuController@footer_menu')->name('admin.footer.menu');
			Route::get('add', 'Admin\MenuController@footer_menu_add')->name('admin.footer.menu.add');
			Route::post('post', 'Admin\MenuController@footer_menu_post')->name('admin.footer.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@footer_menu_edit')->name('admin.footer.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@footer_menu_delete')->name('admin.footer.menu.delete');
		    Route::post('status', 'Admin\MenuController@footer_menu_status')->name('admin.footer.menu.status');
		});

		Route::group(['prefix' => 'sub-footer-menu'], function () {
			Route::get('/{id}', 'Admin\MenuController@footer_sub_menu')->name('admin.footer.sub.menu');
			Route::get('add/{id}', 'Admin\MenuController@footer_sub_menu_add')->name('admin.footer.sub.menu.add');
			Route::post('post', 'Admin\MenuController@footer_sub_menu_post')->name('admin.footer.sub.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@footer_sub_menu_edit')->name('admin.footer.sub.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@footer_sub_menu_delete')->name('admin.footer.sub.menu.delete');
		    Route::post('status', 'Admin\MenuController@footer_sub_menu_status')->name('admin.footer.sub.menu.status');
		});

		Route::group(['prefix' => 'sidebar-menu'], function () {
			Route::get('/', 'Admin\MenuController@sidebar_menu')->name('admin.sidebar.menu');
			Route::get('add', 'Admin\MenuController@sidebar_menu_add')->name('admin.sidebar.menu.add');
			Route::post('post', 'Admin\MenuController@sidebar_menu_post')->name('admin.sidebar.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@sidebar_menu_edit')->name('admin.sidebar.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@sidebar_menu_delete')->name('admin.sidebar.menu.delete');
		    Route::post('status', 'Admin\MenuController@sidebar_menu_status')->name('admin.sidebar.menu.status');
		});

		Route::group(['prefix' => 'sub-sidebar-menu'], function () {
			Route::get('/{id}', 'Admin\MenuController@sidebar_sub_menu')->name('admin.sidebar.sub.menu');
			Route::get('add/{id}', 'Admin\MenuController@sidebar_sub_menu_add')->name('admin.sidebar.sub.menu.add');
			Route::post('post', 'Admin\MenuController@sidebar_sub_menu_post')->name('admin.sidebar.sub.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@sidebar_sub_menu_edit')->name('admin.sidebar.sub.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@sidebar_sub_menu_delete')->name('admin.sidebar.sub.menu.delete');
		    Route::post('status', 'Admin\MenuController@sidebar_sub_menu_status')->name('admin.sidebar.sub.menu.status');
		});
	});


	//leads
	Route::group(['prefix' => 'lead'], function () {
		Route::get('/', 'Admin\LeadController@index')->name('admin.lead');
	});
	//tags
	Route::group(['prefix' => 'tag'], function () {
		Route::get('/', 'Admin\TagController@index')->name('admin.tag.manage');
		Route::get('add', 'Admin\TagController@add')->name('admin.tag.add');
		Route::get('edit/{id}', 'Admin\TagController@edit')->name('admin.tag.edit');
		Route::post('post', 'Admin\TagController@post')->name('admin.tag.post');
		Route::post('delete/{id}', 'Admin\TagController@delete')->name('admin.tag.delete');
	});
	//variants
	Route::group(['prefix' => 'variant'], function () {
		Route::get('/', 'Admin\VariantController@index')->name('admin.variant.manage');
		Route::get('add', 'Admin\VariantController@add')->name('admin.variant.add');
		Route::get('edit/{id}', 'Admin\VariantController@edit')->name('admin.variant.edit');
		Route::post('post', 'Admin\VariantController@post')->name('admin.variant.post');
		Route::post('delete/{id}', 'Admin\VariantController@delete')->name('admin.variant.delete');
	});
	//product
	Route::group(['prefix' => 'product'], function () {
		Route::get('/', 'Admin\ProductController@index')->name('admin.product.manage');
		Route::get('add', 'Admin\ProductController@add')->name('admin.product.add');
		Route::get('edit/{id}', 'Admin\ProductController@edit')->name('admin.product.edit');
		Route::post('post', 'Admin\ProductController@post')->name('admin.product.post');
		Route::post('delete/{id}', 'Admin\ProductController@delete')->name('admin.product.delete');
	});
    
    Route::get('product/create', 'ProductController@create')->name('product.create');
    Route::post('insert', 'ProductController@insert')->name('product.insert');
    Route::post('product/upload/image', 'ProductController@productImageUpload')->name('product.upload.image');
    Route::get('product/remove/image', 'ProductController@removeImage')->name('product.remove.image');
    Route::get('product/manage', 'ProductController@manage')->name('product.manage');
    Route::get('product/details', 'ProductController@details')->name('product.details');
    Route::get('product/edit', 'ProductController@edit')->name('product.edit');
    Route::post('update', 'ProductController@update')->name('product.update');
    Route::post('delete', 'ProductController@delete')->name('product.delete');


    Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('admin.search');


});

Route::group(['prefix' => 'superadmin', 'middleware' => ['role:superadmin', 'auth'],], function () {
	//roles
    Route::get('add-role', 'SuperAdmin\RoleController@add_role')->name('add.role');
	Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('assign.role');

});