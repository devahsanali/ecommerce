function item_delete(id, this_url){      
    swal({
          title: "Do you really want to delete it?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                        url: this_url,
                        type: "post",
                        data: {
                            id:id
                        },
                        success: function(data) {
                            if (data == 'success') {
                                $('#success').show();
                                $('#success-text').html('<strong>Success!</strong> Record has been deleted.');
                                $('.back-to-top').click();
                                setTimeout(
                                  function() 
                                  {
                                    location. reload(true);
                                  }, 1000);                       
                            };
                        },
                        error: function() {
                            alert('There is error while deleting record');
                        }
                    }); 
          } else {

            return false;
          }
        });
}

//update_status
function update_status(id, this_url) {
    if (window.confirm('Do you really want to change it?')){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
                url: this_url,
                type: "post",
                data: {
                    id:id
                },
                success: function(data) {
                    if (data == 'success') {
                        $('#success').show();
                        $('#success-text').html('<strong>Success!</strong> Status has been changed.');
                        $('.back-to-top').click();
                        setTimeout(
                          function() 
                          {
                            location. reload(true);
                          }, 1000);                       
                    };
                },
                error: function() {
                    alert('There is error while changing status');
                }
            }); 
    }
}
function initComp(){
    $('.ajaxFormSubmit').on('click',function(e){
        var form = $('form.ajaxForm');
        var check  = form.valid();
        if(!check){
            $("#loader").hide();
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });
    });

    $("form.ajaxForm").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            $(".loading-wrapper").show();
            $("#loader").show();
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();
            $(".loading-wrapper").hide();

            if(typeof data === 'object'){
                $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");
                $('.back-to-top').click();
              // error("Please fill all mandatory fields.");   
            }else{
                $('#fail').show();
                $('#fail-text').html("Error Occured.Invalid File Format.");
                $('.back-to-top').click();
             //error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                //error(data['error']);
                $('#fail').show();
                $('#fail-text').html(data['error']);
                $('.back-to-top').click();
                $("#loader").hide();
                $(".loading-wrapper").hide();
            }
            else if (data['success'] !== undefined){
                $("#loader").hide();
                $(".loading-wrapper").hide();
                $('#success').show();
                $('#success-text').html(data['success']);
                //success(data['success']);
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){

                resetForm();

            }
        }
    });
}
$(document).ready(function() {
    var form = $('.ajaxForm'),
    original = form.serialize()

    form.submit(function(){
      window.onbeforeunload = null
    })

    window.onbeforeunload = function(){
      if (form.serialize() != original)
        return 'Are you sure you want to leave?'
    }

    initComp();
});
function resetForm(){
    $("form input[type=text]").val("");

    $("form input[type=password]").val("");

    $("form input[type=email]").val("");

    $("form input[type=color]").val("");

    $("form input[type=date]").val("");

    $("form input[type=datetime-local]").val("");

    $("form input[type=file]").val("");

    $("form input[type=image]").val("");

    $("form input[type=month]").val("");

    $("form input[type=number]").val("");

    $("form input[type=range]").val("");

    $("form input[type=tel]").val("");

    $("form input[type=url]").val("");

    $("form input[type=week]").val("");

    $("form select").val("");

    $("form textarea").val("");

}

//switch button
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

elems.forEach(function(html) {
  var switchery = new Switchery(html);
});
//texteditor
$('.summernoteEditor').summernote({
    height: 100,
    tabsize: 2
});
//multiple select
$('.multiple-select').select2();
 /*$(".js-switch").on("change" , function(e) {
    if (window.confirm('Do you really want to delete it?')){
         window.swt = 1;
        alert(true);
    }else{
       $('.js-switch', this).prop('checked', true);

       // console.log("false");
         window.swt = 0;
        return false;
    }
    aler('fuck');
 });*/
 /*(function () {
    var previous;

    $(".js-switch").on('focus', function () {
        // Store the current value on focus and on change
        previous = this.value;
        console.log("old " + this.value);
    }).change(function() {
        // Do something with the previous value after the change
        alert(previous);

        // Make sure the previous value is updated
        previous = this.value;
        console.log("new " + this.value);

    });
})();*/
/*var changeCheckbox = $('.js-switch');

$(".js-switch").on('change', function () {
    
    var id = $(this).data('id'); 
    var url = $(this).data('url'); 
    var status = $(this).checked; 
    console.log(' id ' +$(this).data('id'));
    console.log(' url ' +$(this).data('url'));
      if (window.confirm('Do you really want to change it?')){
            item_delete(id, url);
      }else{
        var switchery = new Switchery(window.elem, { disabled: true });

       if(status){
        $('.js-switch', this).prop('checked', true);
       }else if(!status){
       $('.js-switch', this).prop('checked', false);
       }
      }
   console.log("statuys "+changeCheckbox.checked);
});
*/